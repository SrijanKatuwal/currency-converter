from flask import Flask, render_template, request
import requests

app = Flask(__name__)

def curr_list():
    url = 'https://api.exchangeratesapi.io/latest?base=USD'
    res = requests.get(url)
    result = res.json()["rates"]
    l = []
    for each in result:
        l.append(each)
    return l

def choice(x, y):
    url = 'https://api.exchangeratesapi.io/latest?base='+x
    res = requests.get(url)
    result = res.json()["rates"]
    for each in result:
        if each == y:
            return result[each]

@app.route('/', methods=['POST', 'GET'])
def index():
    sel_from = ''
    sel_to = ''
    if request.method == 'POST':
        x = request.form.get('from')
        y = request.form.get('to')
        inp = int(request.form.get('input'))
        sel_from += x
        sel_to += y
        r = choice(x, y)
        res = inp*r
        list = [curr_list(), [inp, res]]
        return render_template('index.html', list=list, sel_from=sel_from, sel_to=sel_to)    
    list = [curr_list(), (1,1)]
    return render_template('index.html', list=list)

if __name__ == '__main__':
    app.run(debug=True)